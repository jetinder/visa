package com.visa;


import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.RequestResponsePact;
import au.com.dius.pact.core.model.annotations.Pact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visa.model.Address;
import com.visa.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(PactConsumerTestExt.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserControllerRestTemplateTest {

    private static final ObjectMapper om = new ObjectMapper();

    private final static Map<String, String> headers;

    static {
        headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic YWRtaW46cGFzc3dvcmQ=");
    }

    private static final String findUserdetailsResponse = "{\n" +
            "    \"id\": 3,\n" +
            "    \"title\": \"Mr\",\n" +
            "    \"firstn\": \"Peter\",\n" +
            "    \"lastname\": \"Bill\",\n" +
            "    \"gender\": \"male\",\n" +
            "    \"empid\": 1541,\n" +
            "    \"address\": {\n" +
            "        \"street\": \"bobby\",\n" +
            "        \"city\": \"Sydney\",\n" +
            "        \"state\": \"NSW\",\n" +
            "        \"postcode\": 2007\n" +
            "    }\n" +
            "}";

    private static final String postReqeustForUser = "{\n" +
            " \"id\": 3,\n" +
            " \"title\": \"Mr\",\n" +
            " \"firstn\": \"Peter\",\n" +
            " \"lastname\": \"Bill\",\n" +
            " \"gender\": \"male\",\n" +
            " \"empid\": \"3454\",\n" +
            " \"address\": {\n" +
            " \"id\":1,\n" +
            " \"street\": \"bobby\",\n" +
            " \"city\": \"Sydney\",\n" +
            " \"state\": \"NSW\",\n" +
            " \"postcode\": \"2007\"\n" +
            " }\n" +
            " }";

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private UserRepository mockRepository;


    @Before
    public void init() {
        Address address1 = new Address("bobby", "Sydney", "NSW", 2007);
        User user = new User("Mr", "Peter", "Bill", "male", 1541,address1);

        when(mockRepository.findById(1L)).thenReturn(Optional.of(user));

    }

    @Pact(provider = "providerMicroservice", consumer = "consumerMicroservice")
    public RequestResponsePact findUserdetails(PactDslWithProvider builder) {

        return builder
                .given("a user with id 1")
                .uponReceiving("requesting user details")
                .path("/userdetails/1")
                .method("POST")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(findUserdetailsResponse)
                .toPact();
    }

    @Test
    @PactTestFor(pactMethod = "findUserdetails")
    @DisplayName("given a user id of request user details")
    public void findUserdetails() throws Exception {

        String expected = "{\"id\":null,\"title\":\"Mr\",\"firstn\":\"Peter\",\"lastname\":\"Bill\",\"gender\":\"male\",\"empid\":1541,\"address\":{\"street\":\"bobby\",\"city\":\"Sydney\",\"state\":\"NSW\",\"postcode\":2007}}";

        ResponseEntity<String> response = restTemplate
                .withBasicAuth("user", "password")
                .getForEntity("/userdetails/1", String.class);

        printJSON(response);


        assertEquals(MediaType.APPLICATION_JSON_UTF8, response.getHeaders().getContentType());
        assertEquals(expected, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JSONAssert.assertEquals(expected, response.getBody(), false);

    }


    @Test
    public void find_nologin_401() throws Exception {

        String expected = "{\"status\":401,\"error\":\"Unauthorized\",\"message\":\"Unauthorized\",\"path\":\"/api/userdetails/1\"}";

        ResponseEntity<String> response = restTemplate
                .getForEntity("/userdetails/1", String.class);

        printJSON(response);

        assertEquals(MediaType.APPLICATION_JSON_UTF8, response.getHeaders().getContentType());
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());

        JSONAssert.assertEquals(expected, response.getBody(), false);

    }

    private static void printJSON(Object object) {
        String result;
        try {
            result = om.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            System.out.println(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
