package com.visa;

import com.visa.model.User;
import com.visa.model.UserDTO;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserService {

    private static final String RESILIENCE4J_INSTANCE_NAME = "restUpdate";
    private static final String FALLBACK_METHOD = "fallback";

    @Autowired
    private UserRepository repository;

    @Autowired
    private AddressRespository addressRespository;

    public List<User> getAllUsers(){
        List<User> users =repository.findAll();
        return users;
    }

    @CircuitBreaker(name = RESILIENCE4J_INSTANCE_NAME, fallbackMethod = FALLBACK_METHOD)
    @Transactional
    public String updateUser(UserDTO user){
        User userMapped = new User(user.getTitle(),user.getFirstn(), user.getLastname(), user.getGender(), Integer.parseInt(user.getEmpid()) ,user.getAddress());
        addressRespository.save(user.getAddress());
        repository.save(userMapped);
        return "success";
    }

    String fallback(UserDTO user, RuntimeException ex) {
        return "Error" + ex.getMessage();
    }

}
