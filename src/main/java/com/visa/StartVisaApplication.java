package com.visa;

import com.visa.model.Address;
import com.visa.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SpringBootApplication
public class StartVisaApplication {

    // start everything
    public static void main(String[] args) {
        SpringApplication.run(StartVisaApplication.class, args);
    }

    @PersistenceContext
    private EntityManager entityManager;

    @Profile("demo")
    @Bean
    CommandLineRunner initDatabase(UserRepository repository,AddressRespository addressRespository) {

        return args -> {

            Address address1 = new Address("bobby","Sydney","NSW",2007);

            addressRespository.saveAndFlush(address1);
            Address address2 = new Address("bobby2","Sydney","NSW",2007);
            addressRespository.saveAndFlush(address2);
            User user1 = new User("Mr","Peter","Bill","male" , 1541);
            user1.setAddress(address1);
            User user2 = new User("Mrs","susn","Bill","female" ,1541);
            user2.setAddress(address2);

            repository.save(user1);
            repository.save(user2);


            //Address address = new Address("bobby","Sydney","NSW",2007);

        };
    }
}