package com.visa;

import com.visa.error.UserNotFoundException;
import com.visa.model.User;
import com.visa.model.UserDTO;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
public class UserController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserService userService;

    // Find
    @GetMapping("/userdetails")
    List<User> findAll() {

        return userService.getAllUsers();
    }

    // Save
    @PatchMapping("/userdetails")
    @ResponseStatus(HttpStatus.OK)
    String newUser(@Valid @RequestBody UserDTO userDTO) {
        return userService.updateUser(userDTO);

    }

    // Find
    @GetMapping("/userdetails/{id}")
    User findOne(@PathVariable @Min(1) Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }


}
