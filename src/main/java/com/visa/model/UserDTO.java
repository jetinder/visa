package com.visa.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDTO {

    private Long id;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getFirstn() {
        return firstn;
    }

    public String getLastname() {
        return lastname;
    }

    public Address getAddress() {
        return address;
    }

    public String getGender() {
        return gender;
    }

    public String getEmpid() {
        return empid;
    }

    private String title;

    private String firstn;

    public UserDTO(Long id, String title, String firstn, String lastname, String gender, String empid, Address address) {
        this.id = id;
        this.title = title;
        this.firstn = firstn;
        this.lastname = lastname;
        this.gender = gender;
        this.empid = empid;
        this.address = address;
    }

    private String lastname;

    private String gender;

    private String empid;

    private Address address;

}
