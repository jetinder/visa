package com.visa.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;

@Entity
public class User {


    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "Please provide a title")
    private String title;


    @NotEmpty(message = "Please provide a firstName")
    private String firstn;


    @NotEmpty(message = "Please provide a lastName")
    private String lastname;


    @NotEmpty(message = "Please provide a gender")
    private String gender;



    @NotNull(message = "Please provide a employee id")
    @DecimalMin("1.00")
    private Integer empid;

    public User() {
    }



//    public User(Long id, String title, String firstn, BigDecimal price) {
//        this.id = id;
//        this.title = title;
//        this.firstn = firstn;
//        this.empid = price;
//    }
//
//    public User(String name, String author, BigDecimal price) {
//        this.title = name;
//        this.firstn = author;
//        this.empid = price;
//    }

    @OneToOne(cascade = CascadeType.MERGE)
    private Address address;

    public User(String title, String firstn, String lastname, String gender, Integer empid, Address address) {
        this.title = title;
        this.firstn = firstn;
        this.lastname = lastname;
        this.gender = gender;
        this.empid = empid;
        this.address = address;
    }

    public User(String title, String firstn, String lastname, String gender, Integer empid) {
        this.title = title;
        this.firstn = firstn;
        this.lastname = lastname;
        this.gender = gender;
        this.empid = empid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getFirstn() {
        return firstn;
    }

    public void setFirstn(String author) {
        this.firstn = author;
    }

    public Integer getEmpid() {
        return empid;
    }

    public void setEmpid(Integer price) {
        this.empid = price;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", firstn='" + firstn + '\'' +
                ", empid=" + empid +
                '}';
    }
}
